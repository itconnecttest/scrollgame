enchant();
 
window.onload = function() {
    var game = new Game(320,320);
    game.preload('images/chara1.png', 'images/chara2.png', 'images/monster3.gif', 'images/monster4.gif', 
                 'images/connect_man.png',
                 'images/start.png', 'images/gameover.png', 'images/bg1.png', 'images/bg2.png',
                 'images/bg_floor1.png', 'images/bg_floor1.png', 'images/bg_wall1.png', 'images/bg_wall2.png' );
	game.fps = 30;
	game.onload = function() {
        //スタートシーン
        var createStartScene = function() {
            var scene = new Scene();                                // 新しいシーンを作る
            
            // スタート画像設定
            var startImage = new Sprite(236, 48);                   // スプライトを作る
            startImage.image = game.assets['images/start.png'];     // スタート画像を設定
            startImage.x = 42;                                      // 横位置調整
            startImage.y = 80;                                     // 縦位置調整
            scene.addChild(startImage);                             // シーンに追加

            //操作説明
            var ope = new Label();
            ope.moveTo(42, 150);
            scene.addChild(ope);
            ope.text = "敵をよけて進もう！<br>スペース・タップでジャンプ";
            
            // スタート画像にタッチイベントを設定
            startImage.addEventListener(Event.TOUCH_START, function(e) {
                game.replaceScene(createGameScene());    // 現在表示しているシーンをゲームシーンに置き換える
            });
            // タイトルシーンを返します。
            return scene;
        };

        //ゲームシーン
        var createGameScene = function() {
            //シーン生成
            var scene = new Scene();

            //固定値
            var GROUND = 190;   //地面の高さ
            var SPEED = 10; //背景の流れるスピード
            var PHASE = 1; //ゲームの進度

            //背景1の設定
            var bg1 = new Sprite(320, 320);
            bg1.image = game.assets['images/bg1.png'];
            // bg1.image = game.assets['images/bg_floor1.png'];
            bg1.x = 0;
            bg1.y = 0;
            scene.addChild(bg1);

            //背景2の設定
            var bg2 = new Sprite(320, 320);
            bg2.image = game.assets['images/bg2.png'];
            // bg2.image = game.assets['images/bg_floor2.png'];
            bg2.x = 320;
            bg2.y = 0;
            scene.addChild(bg2);

            // var bg1 = new Sprite(320, 320);
            // //bg1.image = game.assets['images/bg1.png'];
            // bg1.image = game.assets['images/bg_wall1.png'];
            // bg1.x = 0;
            // bg1.y = 0;
            // scene.addChild(bg1);

            // //背景2の設定
            // var bg2 = new Sprite(320, 320);
            // //bg2.image = game.assets['images/bg2.png'];
            // bg2.image = game.assets['images/bg_wall2.png'];
            // bg2.x = 320;
            // bg2.y = 0;
            // scene.addChild(bg2);

            //文字表示のためのlabelを生成
            var label = new Label();
            label.moveTo(200, 20);
            scene.addChild(label);

            //スコアを表示(スコア=経過フレーム数)
            var score = 0;
            scene.onenterframe = function() {
                score += 1;
                label.text = "Score : " + score;
            };

            //キー割り当て
            game.keybind(32, 'space');  //ジャンプボタン

            //スマホ画面タップ時の処理
            //タッチされたときにジャンプボタンをオンに
            scene.addEventListener('touchstart', function(){
                game.input.space = true;
            });
            //離したときにオフに
            scene.addEventListener('touchend', function(){
                game.input.space = false;
            });

            //プレイヤー表示クラス
            var Player = Class.create(Sprite, { 
                initialize:function(x,y) {
                    Sprite.call(this,32,32);
                    this.anime = 0;
                    this.x = x;
                    this.y = y;
                    this.v = 0;
                    this.a = 0; //縦軸加速度
                    this.jumping = false;
                    this.image = game.assets['images/connect_man.png'];
                    scene.addChild(this);
                },
                onenterframe:function(){
                    var scene;
                    var p = this.anime%8;

                    switch(p) {
                        case 3: scene = 1; break;
                        case 4: scene = 0; break;
                        case 5: scene = 3; break;
                        case 6: scene = 4; break;
                        case 7: scene = 3; break;
                        default: scene = p;
                    }
                    
                    this.frame = scene;
                    this.anime++;
                    if(game.input.space && !this.jumping){   //ジャンプ
                        this.a = -10.0;
                        // this.a = -15.0;
                        this.jumping = true;
                    }
                    if(this.jumping){   //ジャンプ中は
                        // this.a += 2.0;  //重力を2.0
                        this.a += 1.5;  //重力を1.5
                        this.frame=2;   //ジャンプモーションをさせる
                    }
                    
                    this.v += this.a;
                    this.v = Math.min(Math.max(this.v, -10), 10); //加速しすぎないように
                    this.y += this.v;

                    if(this.y >= GROUND){  //キャラの位置固定
                        this.y = GROUND;
                        this.jumping = false;
                        this.a = 0;
                    }
                }
            });

            //プレイヤー当たり判定用ダミー生成クラス
            //プレイヤーと同じ動きをする
            var P_Dummy = Class.create(Player,{
                initialize:function(x,y){
                    Sprite.call(this,17,27);
                    this.x = x;
                    this.y = y;
                    this.v = 0;
                    this.a = 0; //縦軸加速度
                    this.jumping = false;
                    scene.addChild(this);
                        
                    //色付け
                    //this.backgroundColor = 'blue';
                },
                onenterframe:function(){
                    if(game.input.space && !this.jumping){   //ジャンプ
                        this.a = -10.0;
                        // this.a = -15.0;
                        this.jumping = true;
                    }
                    if(this.jumping){   //ジャンプ中は
                        // this.a += 2.0;  //重力を2.0
                        this.a += 1.5;  //重力を1.5
                    }
                    
                    this.v += this.a;
                    this.v = Math.min(Math.max(this.v, -10), 10); //加速しすぎないように
                    this.y += this.v;

                    if(this.y >= GROUND + 4){  //キャラの位置固定
                        this.y = GROUND + 4;
                        this.jumping = false;
                        this.a = 0;
                    }
                }
            });

            //敵1キャラ表示クラス
            var Enemy = Class.create(Sprite, { 
                initialize:function(x,y) {
                    Sprite.call(this,32,32);
                    this.x = x;
                    this.y = y;
                    this.frame = 0;
                    this.anime = 0;
                    this.scaleX = 1;
                    // this.v = 15 + score/1000;
                    this.v = 7 + score/1000;
                    this.image = game.assets['images/chara2.png'];
                    scene.addChild(this);
                },
                onenterframe:function(){
                    if(game.frame%4 == 0){  //4フレーム毎にアニメーションを切り替える
                        this.frame = this.anime%3;
                        this.anime++;
                    }
                    this.x -= this.v;
                }
            });

            //敵2キャラ表示クラス
            var Enemy2 = Class.create(Sprite, { 
                initialize:function(x,y) {
                    Sprite.call(this,48,48);
                    this.x = x;
                    this.y = y;
                    this.frame = 2;
                    this.anime = 0;
                    this.scaleX = 1;
                    // this.v = 15 + score/1000;
                    this.v = 7 + score/1000;
                    this.image = game.assets['images/monster3.gif'];
                    scene.addChild(this);
                },
                onenterframe:function(){
                    if(game.frame%4 == 0){  //4フレーム毎にアニメーションを切り替える
                        this.frame = this.anime%3+2;
                        this.anime++;
                        console.log(this.frame);
                    }
                    this.x -= this.v;
                }
            });

            //敵3キャラ表示クラス
            var Enemy3 = Class.create(Sprite, { 
                initialize:function(x,y) {
                    Sprite.call(this,48,48);
                    this.x = x;
                    this.y = y;
                    this.frame = 2;
                    this.anime = 0;
                    this.scaleX = 1;
                    // this.v = 15 + score/1000;
                    this.v = 7 + score/1000;
                    this.image = game.assets['images/monster4.gif'];
                    scene.addChild(this);
                },
                onenterframe:function(){
                    if(game.frame%4 == 0){  //4フレーム毎にアニメーションを切り替える
                        this.frame = this.anime%4+2;
                        this.anime++;
                    }
                    this.x -= this.v;
                }
            });

            //敵当たり判定用ダミー生成クラス
            //敵と同じ動きをする
            var E_Dummy = Class.create(Sprite,{
                initialize:function(x,y,obj){
                    Sprite.call(this,24,17);
                    this.x = x;
                    this.y = y;
                    this.obj = obj;
                    // this.v = 15 + score/1000;
                    this.v = 7 + score/1000;
                    scene.addChild(this);
                        
                    //色付け
                    //this.backgroundColor = 'blue';
                },
                onenterframe:function(){
                    this.x -= this.v;
                    if(this.intersect(this.obj)){
                        //game.stop();
                        game.pushScene(createGameoverScene(score));    // 現在表示しているシーンをゲームオーバーシーンに置き換える
                    }
                }
            });

            //プレイヤー表示
            var player = new Player(25,GROUND);
            var player_d = new P_Dummy(33,GROUND + 4);

            //毎フレームの処理
            var time1 = -20;
            scene.addEventListener(Event.ENTER_FRAME, function(){
                //背景の処理
                bg1.x -= SPEED;
                bg2.x -= SPEED;
                if (bg1.x <= -320) {                  // 背景1が画面外に出たら
                    bg1.x = 320;                      // 画面右端に移動
                }
                if (bg2.x <= -320) {                  // 背景2が画面外に出たら
                    bg2.x = 320;                      // 画面右端に移動
                }

                //ゲームの進度を変更
                if(score > 200 && PHASE == 1)
                    PHASE = 2;
                if(score > 600 && PHASE == 2)
                    PHASE = 3;

                //敵出現の処理
                var time2 = game.frame;
                // if(time2-time1 >= 20){  //1つ前の敵出現から20フレーム以上経った時
                if(time2-time1 >= 50){  //1つ前の敵出現から20フレーム以上経った時
                    if(Math.floor(Math.random()*10)%5 == 0){    //20%の確立で敵発生
                        switch(PHASE){
                            case 1:
                                var enemy = new Enemy(370,GROUND);
                                var enemy_d = new E_Dummy(374,GROUND + 13,player_d);
                                //var enemy_d = new E_Dummy(370 + 32 / 2 - 24 / 2, 190 + 32 - 17, player_d);
                                break;
                            
                            case 2:
                                if(Math.floor(Math.random()*10)%2 == 0){
                                    var enemy = new Enemy(370,GROUND);
                                    var enemy_d = new E_Dummy(374,GROUND + 13,player_d);
                                }
                                else{
                                    var enemy = new Enemy2(370,GROUND-16);
                                    var enemy_d = new E_Dummy(380,GROUND-8,player_d);
                                }
                                break;

                            case 3:
                                if(Math.floor(Math.random()*10)%3 == 0){
                                    var enemy = new Enemy(370,GROUND);
                                    var enemy_d = new E_Dummy(374,GROUND+13,player_d);
                                }
                                else if(Math.floor(Math.random()*10)%3 == 1){
                                    var enemy = new Enemy2(370,GROUND-16);
                                    var enemy_d = new E_Dummy(380,GROUND-8,player_d);
                                }
                                else{
                                    var enemy = new Enemy3(370,GROUND-12);
                                    var enemy_d = new E_Dummy(380,GROUND+10,player_d);
                                }
                                break;
                        }
                        time1 = time2;   
                    }                
                }
            });

            //シーンを返す
            return scene;
        };

        //ゲームオーバーシーン
        var createGameoverScene = function(resultScore) {
            var scene = new Scene();                                   // 新しいシーンを作る

            // ゲームオーバー画像設定
            var gameoverImage = new Sprite(189, 97);                   // スプライトを作る
            gameoverImage.image = game.assets['images/gameover.png'];  // ゲームオーバー画像を設定
            gameoverImage.x = 65;                                      // 横位置調整
            gameoverImage.y = 112;                                     // 縦位置調整
            scene.addChild(gameoverImage);                             // シーンに追加
            
            // ゲームオーバー画像にタッチイベントを設定
            gameoverImage.addEventListener(Event.TOUCH_START, function(e) {
                game.popScene();    //ゲームオーバーシーンを取り除く
                game.replaceScene(createStartScene());    //ゲームシーンをタイトルシーンに置き換える
            });
            return scene;
        };
        game.replaceScene(createStartScene());
    }
	game.start();
}
